from django.contrib import admin
from goblins.models import GoblinCommoner, GoblinScout, GoblinThief, GoblinSorcerer, GoblinWarlord

# Register your models here.

admin.site.register(GoblinCommoner)
admin.site.register(GoblinScout)
admin.site.register(GoblinThief)
admin.site.register(GoblinSorcerer)
admin.site.register(GoblinWarlord)
