from django.db import models
import random

# Create your models here.

d20 = random.randint(1, 20)
d10 = random.randint(1, 10)
d8 = random.randint(1, 8)
d6 = random.randint(1, 6)
d4 = random.randint(1, 4)

class GoblinCommoner(models.Model):
    name = "Wink"
    title = "Goblin"
    armor_class = 10
    hit_points = 3
    attack = d20 + 1
    damage = d4
    weapon = "club"

class GoblinScout(models.Model):
    name = "Skort"
    title = "Goblin Scout"
    armor_class = 13
    hit_points = 7
    attack = d20 + 4
    damage = d6 + 1
    weapon = "shortbow"

class GoblinThief(models.Model):
    name = "Nott"
    title = "Goblin Thief"
    armor_class = 15
    hit_points = 9
    attack = d20 + 4
    damage = d6 + 2
    weapon = "shortblade"

class GoblinSorcerer(models.Model):
    name = "Valt"
    title = "Goblin Sorcerer"
    armor_class = 15
    hit_points = 14
    attack = d20 + 5
    damage = d4 + 2
    weapon = "dagger"
    spell_attack = d20 + 6
    spell_damage = d10
    spell = "fire bolt"

class GoblinWarlord(models.Model):
    name = "Chizguk"
    title = "Warlord of the Goblins"
    armor_class = 16
    hit_points = 25
    attack = d20 + 6
    damage = d8 + 4
    weapon = "longsword"
    second_attack = d20 + 4
    second_damage = d10 + 6
    second_weapon = "warring strike"


