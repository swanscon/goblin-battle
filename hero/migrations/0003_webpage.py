# Generated by Django 4.1 on 2022-08-10 22:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("hero", "0002_herolevel1_user"),
    ]

    operations = [
        migrations.CreateModel(
            name="WebPage",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
            ],
        ),
    ]
