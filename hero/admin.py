from django.contrib import admin
from hero.models import HeroLevel1, HeroLevel2, HeroLevel3, HeroLevel4, HeroLevel5

# Register your models here.

admin.site.register(HeroLevel1)
admin.site.register(HeroLevel2)
admin.site.register(HeroLevel3)
admin.site.register(HeroLevel4)
admin.site.register(HeroLevel5)
