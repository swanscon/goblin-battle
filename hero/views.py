from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from hero.models import HeroLevel1, WebPage
from django.views.generic.base import TemplateView

# Create your views here.

class HeroCreateView(LoginRequiredMixin, CreateView):
    model = HeroLevel1
    context_object_name = "hero"
    template_name = "hero/create.html"
    fields = ["name"]
    
    def get_success_url(self):
        return reverse_lazy("choose_weapon")


class WeaponView(TemplateView):
    template_name = "hero/choose.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['choose_weapon'] = WebPage.objects.all()
        return context