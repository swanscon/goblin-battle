from django.urls import path

from hero.views import (
    HeroCreateView,
    WeaponView,
)


urlpatterns = [
    path("create/", HeroCreateView.as_view(), name="hero_create"),
    path("choose/", WeaponView.as_view(), name="choose_weapon"),
]