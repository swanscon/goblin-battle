from django.db import models
import random
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL

d20 = random.randint(1, 20)
d12 = random.randint(1, 12)
d10 = random.randint(1, 10)
d8 = random.randint(1, 8)
d6 = random.randint(1, 6)
d4 = random.randint(1, 4)

# Create your models here.

class WebPage(models.Model):
    title = "Goblin Battle"

class HeroLevel1(models.Model):
    name = models.CharField(max_length=25)
    level = 1
    hit_points = 10
    armor_class = 12
    player = models.ForeignKey(
        USER_MODEL,
        related_name="hero",
        on_delete=models.CASCADE,
        null=True,
    )

class HeroLevel2(models.Model):
    name = HeroLevel1.name
    level = 2
    hit_points = 12
    armor_class = 13
    user = HeroLevel1.player

class HeroLevel3(models.Model):
    name = HeroLevel1.name
    level = 3
    hit_points = 14
    armor_class = 14
    user = HeroLevel1.player

class HeroLevel4(models.Model):
    name = HeroLevel1.name
    level = 4
    hit_points = 16
    armor_class = 15
    user = HeroLevel1.player

class HeroLevel5(models.Model):
    name = HeroLevel1.name
    level = 5
    hit_points = 18
    armor_class = 16
    user = HeroLevel1.player

shortsword = d6 + 2
battle_axe = d8 + 2
longbow = d8 + 2

shortsword2 = d6 + 3
greatsword = d10 + 3
heavy_crossbow = d10 + 3

def special_attack():
    adv_roll = 0
    roll1 = d20 + 6
    roll2 = d20 + 6
    if roll1 > roll2:
        adv_roll = roll1
    else:
        adv_roll = roll2
    return adv_roll

shield_bash = d10 + 4
magic_bolt = d12 + 4
charging_strike = d12 + 4
