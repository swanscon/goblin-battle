from django.shortcuts import render
from django.views.generic.base import TemplateView
from battle.models import WebPage

# Create your views here.

class HomeView(TemplateView):
    template_name = "battle/home.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['home'] = WebPage.objects.all()
        return context

class RulesView(TemplateView):
    template_name = "battle/rules.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['rules'] = WebPage.objects.all()
        return context

class SwordView1(TemplateView):
    template_name = "level1/sword.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['level1_sword'] = WebPage.objects.all()
        return context

class BowView1(TemplateView):
    template_name = "level1/bow.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['level1_bow'] = WebPage.objects.all()
        return context

class AxeView1(TemplateView):
    template_name = "level1/axe.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['level1_axe'] = WebPage.objects.all()
        return context