from django.contrib import admin
from battle.models import Battle1, Battle2, Battle3, Battle4, Battle5

# Register your models here.

admin.site.register(Battle1)
admin.site.register(Battle2)
admin.site.register(Battle3)
admin.site.register(Battle4)
admin.site.register(Battle5)
