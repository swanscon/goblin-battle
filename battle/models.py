from django.db import models
import random
from goblins.models import GoblinCommoner, GoblinScout, GoblinThief, GoblinSorcerer, GoblinWarlord
from hero.models import HeroLevel1, HeroLevel2, HeroLevel3, HeroLevel4, HeroLevel5, shortsword, battle_axe, longbow, greatsword, heavy_crossbow, shield_bash, magic_bolt, charging_strike

# Create your models here.

d20 = random.randint(1, 20)
d12 = random.randint(1, 12)
d10 = random.randint(1, 10)
d8 = random.randint(1, 8)
d6 = random.randint(1, 6)
d4 = random.randint(1, 4)

class WebPage(models.Model):
    title = "Goblin Battle"

class Battle1(models.Model):
    enemy_name = GoblinCommoner.name
    enemy_health = GoblinCommoner.hit_points
    enemy_armor = GoblinCommoner.armor_class
    player_name = HeroLevel1.name
    player_health = HeroLevel1.hit_points
    player_armor = HeroLevel1.armor_class

    def initiative():
        pass

    def enemy_attack():
        result = GoblinCommoner.attack
        if result == 20:
            pass
        elif result >= HeroLevel1.armor_class:
            pass

    def player_attack():
        pass

class Battle2(models.Model):
    enemy_health = GoblinScout.hit_points
    enemy_armor = GoblinScout.armor_class
    player_health = HeroLevel2.hit_points
    player_armor = HeroLevel2.armor_class

class Battle3(models.Model):
    enemy_health = GoblinThief.hit_points
    enemy_armor = GoblinThief.armor_class
    player_health = HeroLevel3.hit_points
    player_armor = HeroLevel3.armor_class

class Battle4(models.Model):
    enemy_health = GoblinSorcerer.hit_points
    enemy_armor = GoblinSorcerer.armor_class
    player_health = HeroLevel4.hit_points
    player_armor = HeroLevel4.armor_class

class Battle5(models.Model):
    enemy_health = GoblinWarlord.hit_points
    enemy_armor = GoblinWarlord.armor_class
    player_health = HeroLevel5.hit_points
    player_armor = HeroLevel5.armor_class