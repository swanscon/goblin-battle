from django.urls import path
from battle.views import (
    HomeView,
    RulesView
)

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("rules.html", RulesView.as_view(), name="rules"),
    path("level1/sword.html", RulesView.as_view(), name="level1_sword"),
    path("level1/bow.html", RulesView.as_view(), name="level1_bow"),
    path("level1/axe.html", RulesView.as_view(), name="level1_axe"),
]
